/*
 * @creation 1998-04-22
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dunes;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.calcul.SProgression;
import org.fudaa.dodico.corba.dunes.ICalculDunes;
import org.fudaa.dodico.corba.dunes.ICalculDunesOperations;
import org.fudaa.dodico.corba.dunes.IParametresDunes;
import org.fudaa.dodico.corba.dunes.IParametresDunesHelper;
import org.fudaa.dodico.corba.dunes.IResultatsDunes;
import org.fudaa.dodico.corba.dunes.IResultatsDunesHelper;
import org.fudaa.dodico.corba.geometrie.LTypeElement;
import org.fudaa.dodico.corba.objet.IConnexion;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.objet.CDodico;

/**
 * Classe d'implantation de l'interface <code>ICalculDunes</code> gerant l'execution du code de calcul
 * <code>dunes</code>.
 * 
 * @version $Revision: 1.22 $ $Date: 2006-09-19 14:42:29 $ by $Author: deniger $
 * @author Sofiane Hadji , Bertrand Marchand
 */
public class DCalculDunes extends DCalcul implements ICalculDunes, ICalculDunesOperations {
  private boolean arretDemande_;
  private Process proc_; // Sous process d'ex�cution du programme.

  /**
   * Structure decrivant l'etat d'avancement du calcul.
   */
  private final SProgression strOperation_;
  private boolean succes_;
  private StringBuffer traceBuffer_ = new StringBuffer();
  /**
   * Definit l'aire maxi autorisee pour les elements.Valeur par defaut : 0.
   */
  double aireMax_;
  /**
   * Definit l'angle mini autorise pour les elements.Valeur par defaut : 0.
   */
  double angleMin_;
  /**
   * Definit le type d'element pour le mailleur. Valeur par defaut : T3.
   */
  LTypeElement elem_;
  /**
   * Hauteur de mer pour le maillage par longueur d'onde. Valeur par defaut : 1.
   */
  double hauteur_;
  /**
   * Nombre de points par longueur d'onde pour le maillage par longueur d'onde. Valeur par defaut : 1.
   */
  int nbPoints_;
  /**
   * Si vrai : contr�le de la qualite des aires actif. Valeur par defaut :<code>true</code>.
   */
  boolean optionA_;
  /**
   * Si vrai : ajout d'une enveloppe convexe. Valeur par defaut :<code>false</code>.
   */
  boolean optionC_;
  /**
   * Si vrai : activation du maillage par longueur d'onde. Valeur par defaut :<code>false</code>.
   */
  boolean optionO_;
  /**
   * Prise en compte des polylignes lors du maillage: toujours vrai. Valeur par defaut : <code>true</code>.
   */
  boolean optionP_;
  /**
   * Si vrai : controle de la qualite des angles actif. Valeur par defaut :<code>true</code>.
   */
  boolean optionQ_;
  /**
   * Si vrai: pas de rajout des points sur les polylignes. Valeur par defaut :<code>false</code>.
   */
  boolean optionS_;
  /**
   * Si vrai : Pas de rajout de noeuds sur les contours. Valeur par defaut :<code>false</code>.
   */
  boolean optionY_;
  /**
   * Periode de la houle pour le maillage par longueur d'onde. Valeur par defaut : 1.
   */
  double periode_;

  /**
   * Initialisation de la structure de l'etat d'avancement (mise a zero), des extensions (".dunes_out" et ".dunes_geo")
   * et des variables internes (valeurs par defaut).
   */
  public DCalculDunes() {
    super();
    strOperation_ = new SProgression("", 0);
    setFichiersExtensions(new String[] { ".dunes_out", ".dunes_geo" });
    elem_ = LTypeElement.T3;
    optionP_ = true;
    optionQ_ = true;
    angleMin_ = 0.0;
    optionA_ = true;
    aireMax_ = 0.0;
    optionS_ = false;
    optionC_ = false;
    optionO_ = false;
    hauteur_ = 1;
    nbPoints_ = 1;
    periode_ = 1;
    optionY_ = false;
  }

  /**
   * Modifie la structure decrivant l'avancement du calcul.
   * 
   * @param _op chaine decrivant l'operation (peut etre null).
   * @param _pc l'etat d'avancement represente en pourcentage.
   */
  private synchronized void setProgression(final String _op, final int _pc) {
    strOperation_.operation = _op;
    if (strOperation_.operation == null) {
      strOperation_.operation = CtuluLibString.EMPTY_STRING;
    }
    strOperation_.pourcentage = _pc;
  }

  /**
   * Renvoie l'aire maximale autorisee pour les elements. Cette aire peut varier entre [0,+infini[. Tout autre valeur
   * n'a pas de sens. Si le contr�le de qualite n'est pas active (option A), cette methode n'a pas d'effet. L'aire maxi
   * vaut par defaut 0. Dans ce cas, <code>dunes</code> prend une aire moyenne de contr�le.
   * 
   * @return aire maximale a respecter.
   * @see #optionA()
   */
  public double aireMax() {
    return aireMax_;
  }

  /**
   * Modifie l'aire maximale a respecter.
   * 
   * @param _aireMax Nouvelle aire maxi a respecter.
   * @see #aireMax()
   */
  public void aireMax(final double _aireMax) {
    aireMax_ = _aireMax;
  }

  /**
   * Definit l'angle (en degre) mini autorise pour les elements. Cet angle peut varier entre 0 et 60� : tout autre angle
   * n'a pas de sens. Si le contr�le de qualite n'est pas active (option Q), cette methode n'a pas d'effet. L'angle vaut
   * par defaut 0. Dans ce cas, <code>dunes</code> prend un angle par defaut � 21,4�.
   * 
   * @return l'angle minimum a respecter en cas de controle des angles.
   */
  public double angleMin() {
    return angleMin_;
  }

  /**
   * Modifie la valeur de l'angle minimum a respecter.
   * 
   * @param _angleMin nouvelle valeur de l'angle en degre (en 0 et 60�).
   * @see #angleMin()
   */
  public void angleMin(final double _angleMin) {
    angleMin_ = _angleMin;
  }

  /**
   * Apres verification de la connexion et de l'existence des interfaces de Parametres et de Resultats, lance le code de
   * calcul. Le fichier de resultats obtenu est affecte a l'interface <code>IResultatsDunes</code> qui pourra
   * l'exploiter par la suite.
   * 
   * @param _c connexion utilisee pour lancer ce calcul.
   */
  public void calcul(final IConnexion _c) {
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("debut calcul");
    }
    succes_ = false;
    if (!verifieConnexion(_c)) {
      return;
    }
    final IParametresDunes params = IParametresDunesHelper.narrow(parametres(_c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
      return;
    }
    final IResultatsDunes results = IResultatsDunesHelper.narrow(resultats(_c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
      return;
    }
    results.clearResultats();
    log(_c, "lancement du calcul");
    // traceBuffer_=new StringBuffer();
    String options = "";
    String optionO = "";
    String ssOptionO = "";
    final String fichier = "dunes" + _c.numero();
    final String os = System.getProperty("os.name");
    final String path = cheminServeur();
    // Options de calcul
    options = getOptions(options);
    if (optionO_) {
      optionO = "-O";
      ssOptionO += "n" + nbPoints_ + "h" + hauteur_ + "p" + periode_;
    }
    try {
      if (arretDemande_) {
        return;
      }
      setProgression("Ecriture des param�tres", 5);
      FuLog.all("Ecriture des param�tres pour " + fichier);
      DParametresDunes.ecritParametresDunes(path + fichier, params);
      FuLog.all("Fin de l'ecriture");
      String exe;
      if (os.startsWith("Windows")) {
        exe = "dunes.exe";
      } else {
        exe = "dunes-" + os + ".x";
      }
      setProgression("Calcul", 10);
      FuLog.all("Ex�cution de dunes options : " + options + " " + optionO + " " + ssOptionO + " pour " + fichier);
      final String[] cmd = new String[optionO_ ? 6 : 4];
      int icmd = 0;
      cmd[icmd++] = path + exe;
      cmd[icmd++] = options;
      if (optionO_) {
        cmd[icmd++] = optionO;
        cmd[icmd++] = ssOptionO;
      }
      cmd[icmd++] = fichier + ".dunes_geo";
      cmd[icmd++] = fichier + ".dunes_out";
      setProgression("Calcul", 12);
      final StringBuffer cmdStrBuf = new StringBuffer(20);
      for (int i = 0; i < cmd.length; i++) {
        cmdStrBuf.append(cmd[i]).append(CtuluLibString.ESPACE);
      }
      final String cmdStr = cmdStrBuf.toString();
      System.err.println("Command: " + cmdStr);
      if (arretDemande_) {
        return;
      }
      proc_ = Runtime.getRuntime().exec(cmd, null, new File(path));
      // 2 thread sont crees pour lire les sorties du Process : le buffer sera
      // ainsi vide et le process peut se terminer.
      if (arretDemande_) {
        proc_.destroy();
        return;
      }
      final InputStreamReader perr = new InputStreamReader(proc_.getErrorStream());
      final InputStreamReader pout = new InputStreamReader(proc_.getInputStream());
      final StringBuffer sResultRunnable = new StringBuffer("");
      final StringBuffer traceBufferRunnable = new StringBuffer("");
      // Lancement de 2 thread pour lire les flux du process
      final Runnable sortieStandardRunnable = new Runnable() {

        public void run() {
          int chOut;
          try {
            while ((chOut = pout.read()) != -1) {
              traceBufferRunnable.append((char) chOut);
            }
          } catch (IOException e) {
            FuLog.all("erreur de lecture pour le flux normal du process dunes");
            FuLog.error(e);
          }
        }
      };
      final Runnable sortieErreurRunnable = new Runnable() {

        public void run() {
          int chErr;
          try {
            while ((chErr = perr.read()) != -1) {
              sResultRunnable.append((char) chErr);
            }
            FuLog.all("Fin lecture sortie d'erreur");
          } catch (IOException e) {
            FuLog.all("erreur de lecture pour le flux d'erreurs du process dunes");
            FuLog.error(e);
          }
        }
      };
      final Thread sortieStandardThread = new Thread(sortieStandardRunnable);
      // sortieStandardThread.setPriority(Thread.MAX_PRIORITY);
      final Thread sortieErreurThread = new Thread(sortieErreurRunnable);
      // sortieErreurThread.setPriority(Thread.MAX_PRIORITY);
      sortieStandardThread.start();
      sortieErreurThread.start();
      proc_.waitFor();
      traceBuffer_ = traceBufferRunnable;
      String sResult;
      sResult = sResultRunnable.toString();
      traceBuffer_.append(sResult);
      /** fin nouvelle version* */
      // Succ�s du calcul si aucune erreur et fichier r�sultats existant.
      if (proc_.exitValue() == 0) {
        succes_ = new File(path + fichier + ".dunes_out").length() != 0;
      }
      // Le calcul s'est mal d�roul�.
      if (!succes_) {
        FuLog.all("Erreurs :" + sResult);
        FuLog.all("Erreurs : le fichier de sortie de dunes est vide");
        return;
      }
      results.setFichier(path + fichier);
      FuLog.all("Fin du calcul");
      log(_c, "calcul termine");
      setProgression(null, 0);
    } catch (final Exception ex) {
      setProgression(null, 0);
      log(_c, "erreur du calcul");
      CDodico.exception(this, ex);
    } finally {
      if (arretDemande_) {
        traceBuffer_.append("\n*** Interruption utilisateur ***\n");
        arretDemande_ = false;
      }
    }
  }

  private String getOptions(final String _initOPt) {
    String res = _initOPt;
    if (optionP_) {
      res += "p";
    }
    if (optionC_) {
      res += "c";
    }
    if (optionS_) {
      res += "S";
    }
    if (optionY_) {
      res += "Y";
    }
    if (elem_ == LTypeElement.T6) {
      res += "T6";
    }
    // else options+="T3";
    if (optionQ_) {
      res += "q";
      if (angleMin_ != 0.0) {
        res += angleMin_;
      }
    }
    if (optionA_) {
      res += "a";
      if (aireMax_ != 0.0) {
        res += aireMax_;
      }
    }
    return res;
  }

  /**
   * Annulation de l'arret demand�.
   */
  public void clearArretDemande() {
    arretDemande_ = false;
  }

  /**
   * Description du serveur de calcul.
   * 
   * @return description du code de calcul+ Systeme d'exploitation.
   */
  public String description() {
    return "Dunes, serveur de maillage: " + super.description();
  }

  /**
   * Retourne l'�tat de sortie du calcul. Le calcul s'est correctement d�roul� si des r�sultats existent.
   * 
   * @return <i>true </i> Le calcul s'est bien d�roul�. <i>false </i> sinon.
   */
  public boolean estOK() {
    return succes_;
  }

  /**
   * Controle si le serveur est operationnel : si l'executable est disponible.
   * 
   * @return <code>true</code> si operationnel.
   */
  public boolean estOperationnel() {
    boolean op;
    final String os = System.getProperty("os.name");
    final String path = cheminServeur();
    if (os.startsWith("Windows")) {
      op = new File(path + "dunes.exe").exists();
    } else {
      op = new File(path + "dunes-" + os + ".x").exists();
    }
    return op;
  }

  /**
   * renvoie la hauteur de mer pour le maillage par longueur d'onde. Si le maillage par longueur d'onde n'est pas
   * active, cette methode n'a pas d'effet. Par defaut, vaut 1.
   * 
   * @return La hauteur de mer.
   */
  public double hauteurMer() {
    return hauteur_;
  }

  /**
   * Modifie la hauteur de la mer.
   * 
   * @param _hauteur nouvelle valeur strictement positive.
   */
  public void hauteurMer(final double _hauteur) {
    hauteur_ = _hauteur;
  }

  /**
   * Renvoie le nombre de points par longueur d'onde pour le maillage par longueur d'onde. Si le maillage par longueur
   * d'onde n'est pas active, cette methode n'a pas d'effet. Par defaut, il vaut 1.
   * 
   * @return le nombre de point par longueur d'onde.
   */
  public int nombrePointsLongueurOnde() {
    return nbPoints_;
  }

  /**
   * Modifie le nombre de point par longueur d'onde.
   * 
   * @param _nbPoints la nouvelle valeur strictement positive.
   * @see #nombrePointsLongueurOnde()
   */
  public void nombrePointsLongueurOnde(final int _nbPoints) {
    nbPoints_ = _nbPoints;
  }

  /**
   * Contr�le de la qualite de l'aire. Quand cette option est activee, <code>Dunes</code> tente de respecter un
   * maximum pour les aires sur les elements :<code>aireMax</code>. Par defaut, le contr�le est actif (<code>true</code>).
   * 
   * @return <code>false</code> controle de qualite des aires inactif <br>
   *         <code>true</code> contr�le actif
   * @see #aireMax()
   */
  public boolean optionA() {
    return optionA_;
  }

  /**
   * Modifie la valeur de l'option A.
   * 
   * @param _optionA nouvelle valeur de l'option.
   * @see #aireMax(double)
   * @see #optionA()
   */
  public void optionA(final boolean _optionA) {
    optionA_ = _optionA;
  }

  /**
   * Ajout ou non d'une enveloppe convexe contenant tous les points sans consideration de contour exterieur definit
   * depuis les polylignes. Les elements generes ne se limitent pas au contour exterieur eventuel. Par defaut, l'option
   * vaut <code>false</code>.
   * 
   * @return <code>true</code> rajout d'une enveloppe exterieur. <code>false</code> pas de rajout d'enveloppe
   *         exterieur.
   */
  public boolean optionC() {
    return optionC_;
  }

  /**
   * Modifie l'option C.
   * 
   * @param _optionC nouvelle valeur de l'option.
   * @see #optionC()
   */
  public void optionC(final boolean _optionC) {
    optionC_ = _optionC;
  }

  /**
   * Activation du maillage par longueur d'onde. Le maillage est effectue avec au prealable un traitement pour le
   * respect de la longueur d'onde. Par defaut, l'option vaut <code>false</code>.
   * 
   * @return <code>true</code> respect de la longueur d'onde <code>false</code> longueur d'onde non prise en compte
   */
  public boolean optionO() {
    return optionO_;
  }

  /**
   * Modifie l'option 0.
   * 
   * @param _optionO nouvelle valeur de l'option.
   * @see #optionO()
   */
  public void optionO(final boolean _optionO) {
    optionO_ = _optionO;
  }

  /**
   * Prise en compte des polylignes lors du maillage: toujours vrai.
   * 
   * @return true les polylignes sont prises en compte
   */
  public boolean optionP() {
    return optionP_;
  }

  /**
   * Prise en compte des polylignes lors du maillage :obligatoire donc toujours vrai. Cette methodes est inactive.
   */
  public void optionP(final boolean _optionP) {
  // optionP_ = _optionP;
  }

  /**
   * Contr�le de la qualite des angles. Quand cette option est activee, <code>Dunes</code> tente de respecter des
   * angles sur les elements tels que anglesElement>angleMin. Par defaut, le contr�le de qualite est actif (true)
   * 
   * @return <code>false</code> Pas de controle de qualite des angles <br>
   *         <code>true</code> Contr�le de qualite des angles actif
   * @see #angleMin(double)
   * @see #optionQ(boolean)
   */
  public boolean optionQ() {
    return optionQ_;
  }

  /**
   * Modifie l'option Q : controle de la qualite des angles.
   * 
   * @param _optionQ nouvelle valeur de l'option
   * @see #angleMin(double)
   * @see #optionQ()
   */
  public void optionQ(final boolean _optionQ) {
    optionQ_ = _optionQ;
  }

  /**
   * Autorise ou non le rajout de points sur les polylignes (qu'elles soient de contrainte ou de contour). Par defaut,
   * cette option vaut <code>false</code>.
   * 
   * @return <code>true</code> pas de rajout de point sur les polylignes <code>false</code> rajout de points en
   *         fonction des crit�res de qualite definis
   */
  public boolean optionS() {
    return optionS_;
  }

  /**
   * Modifie la valeur de l'option S.
   * 
   * @param _optionS nouvelle valeur de l'option.
   * @see #optionS()
   */
  public void optionS(final boolean _optionS) {
    optionS_ = _optionS;
  }

  /**
   * Autorise ou non l'ajout de noeuds sur les contours. Si <code>true</code>, l'ajout est impossible. Valeur par
   * defaut :<code>false</code>.
   * 
   * @return <code>true</code> ajout impossible <br>
   *         <code>false</code> ajout possible.
   */
  public boolean optionY() {
    return optionY_;
  }

  /**
   * Modifie l'option Y qui autorise ou non l'ajout de noeuds.
   * 
   * @param _optionY nouvelle valeur de l'option Y
   * @see #optionY()
   */
  public void optionY(final boolean _optionY) {
    optionY_ = _optionY;
  }

  /**
   * Renvoie la periode de la houle pour le maillage par longueur d'onde. Si le maillage par longueur d'onde n'est pas
   * active, cette methode n'a pas d'effet. Par defaut, elle vaut 1.
   * 
   * @return Periode de la houle
   */
  public double periodeHoule() {
    return periode_;
  }

  /**
   * Modifie la periode de la houle.
   * 
   * @param _periode nouvelle periode de la houle.
   * @see #periodeHoule()
   */
  public void periodeHoule(final double _periode) {
    periode_ = _periode;
  }

  /**
   * Structure decrivant l'etat d'avancement du calcul.
   */
  public SProgression progression() {
    return strOperation_;
  }

  /**
   * Arret demand� du calcul en cours.
   */
  public void setArretDemande() {
    arretDemande_ = true;
    // Arret du sous process s'il est en cours.
    if (proc_ != null) {
      proc_.destroy();
    }
  }

  /**
   * Description de l'objet.
   * 
   * @return "DCalculDunes()"
   */
  public String toString() {
    return "DCalculDunes()";
  }

  /**
   * Retourne la trace d'ex�cution sous forme de chaine. Les lignes sont s�par�es par des caract�res de fin de ligne
   * "\n".
   * 
   * @return La trace d'ex�cution.
   */
  public String traceExecution() {
    return traceBuffer_.toString();
  }

  /**
   * Retourne le type d'element de maillage. Par defaut, le maillage est effectue avec le type
   * <code>LTypeElement.T3</code>.
   */
  public LTypeElement typeElementDunes() {
    return elem_;
  }

  /**
   * Modifie le Type d'Element utilise pour le maillage.
   * 
   * @param _elem le nouveau type d'element utilise.
   */
  public void typeElementDunes(final LTypeElement _elem) {
    elem_ = _elem;
  }
}