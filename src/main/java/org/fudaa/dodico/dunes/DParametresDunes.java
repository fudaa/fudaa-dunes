/*
 * @creation     1998-04-15
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.dunes;

import java.io.FileWriter;
import java.io.IOException;

import com.memoire.fu.FuLog;

import org.fudaa.dodico.corba.dunes.IParametresDunes;
import org.fudaa.dodico.corba.dunes.IParametresDunesOperations;
import org.fudaa.dodico.corba.geometrie.SPoint;
import org.fudaa.dodico.corba.geometrie.SPolyligne;
import org.fudaa.dodico.corba.geometrie.SRegion;
import org.fudaa.dodico.corba.geometrie.STrou;

import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.geometrie.SPointVecteur;
import org.fudaa.dodico.objet.CDodico;

/**
 * Classe d'implantation de l'interface <code>IParametresDunes</code> gerant les parametres du code de calcul
 * <code>dunes</code>.
 *
 * @version $Id: DParametresDunes.java,v 1.12 2006-09-19 14:42:29 deniger Exp $
 * @author Sofiane Hadji , Guillaume Desnoix, Bertrand Marchand
 */
public class DParametresDunes extends DParametres implements IParametresDunes, IParametresDunesOperations {
  private STrou[] sT_;
  private SRegion[] sR_;
  private SPoint[] sPt_;
  private SPolyligne[] sPoly_;

  /**
   * Initialise toutes les structures internes (points, polylignes,trous et regions) et le fichier cible (ecriture des
   * parametres). cf dunes.idl pour des precisions sur les structures utilisees.
   */
  public DParametresDunes() {
    super();
    sT_ = new STrou[0];
    sR_ = new SRegion[0];
    sPt_ = new SPoint[0];
    sPoly_ = new SPolyligne[0];
  }

  /**
   * Description de l'objet.
   */
  public String toString() {
    String s = "DParametresDunes(" + sPt_.length + " points";
    if (sPoly_.length > 0) {
      s += ", " + sPoly_.length + " polylignes";
    }
    if (sT_.length > 0) {
      s += ", " + sT_.length + " trous";
    }
    if (sR_.length > 0) {
      s += ", " + sR_.length + " regions";
    }
    s += ")";
    return s;
  }

  /**
   * Modifie la structure decrivant les trous.
   *
   * @param _t le nouveau tableau de trous.
   */
  public void trous(final STrou[] _t) {
    sT_ = _t;
  }

  /**
   * Renvoie la strucutre decrivant les trous a prendre en compte dans le maillage.
   */
  public STrou[] trous() {
    return sT_;
  }

  /**
   * Modifie la structure decrivant les regions.
   *
   * @param _r le nouveau tableau de regions.
   */
  public void regions(final SRegion[] _r) {
    sR_ = _r;
  }

  /**
   * Les regions du maillage.
   */
  public SRegion[] regions() {
    return sR_;
  }

  /**
   * Modifie les points du maillage.
   *
   * @param _pt le nouveau tableau de points.
   */
  public void points(final SPoint[] _pt) {
    sPt_ = _pt;
  }

  /**
   * Les points du maillage.
   */
  public SPoint[] points() {
    return sPt_;
  }

  /**
   * Modifie la structure decrivant les polylignes.
   *
   * @param _poly le nouveau tableau de polylignes.
   */
  public void polylignes(final SPolyligne[] _poly) {
    sPoly_ = _poly;
  }

  /**
   * Les polylignes du maillage.
   */
  public SPolyligne[] polylignes() {
    return sPoly_;
  }

  /**
   * Ecrit les parametres <code>_param</code> dans le fichier "<code>_fichier</code>.dunes_geo". L'unicite des
   * structures ecrites est assuree. Cette methode reste a etre optimisee.
   *
   * @param _fichier le nom du fichier cible sans extension
   * @param _param l'interface a ecrire dans le fichier
   */
  public static void ecritParametresDunes(final String _fichier, final IParametresDunes _param) {
    STrou[] trs;
    SRegion[] rgs;
    SPoint[] pts;
    SPolyligne[] pls;
    trs = _param.trous();
    rgs = _param.regions();
    pts = _param.points();
    pls = _param.polylignes();
    /** * */
    /** * Creer un objet de type Vector et y * */
    /** * mettre des objets de type point * */
    /** * */
    // Vector indexs = new Vector(0,1);
    final int n = 3 * pls.length + pts.length;
    final SPointVecteur indexs = new SPointVecteur(n, n / 10);
    // Ajoute les points des polylignes (assure l'unicite)
    for (int i = pls.length - 1; i >= 0; i--) {
      final SPoint[] ptsPl = pls[i].points;
      for (int j = ptsPl.length - 1; j >= 0; j--) {
        indexs.addElement(ptsPl[j]);
      }
    }
    for (int i = pts.length - 1; i >= 0; i--) {
      indexs.addElement(pts[i]);
    }
    FuLog.all("NbPts : " + indexs.size());
    /** * */
    /** * Ecriture dans le fichier *.dunes_geo * */
    /** * */
    FuLog.all("Ecriture de " + _fichier + ".dunes_geo");
    writeFile(_fichier, trs, rgs, pls, indexs);
  }

  private static void writeFile(final String _fichier, final STrou[] _trs, final SRegion[] _rgs, final SPolyligne[] _pls, final SPointVecteur _indexs) {
    SPoint[] pts;
    try {
      final FortranWriter fout = new FortranWriter(new FileWriter(_fichier + ".dunes_geo"));
      int[] fmt;
      /** * */
      /** Ecriture des coordonnees des points * */
      /** * */
      fmt = new int[] { 8, 8 };
      fout.intField(0, _indexs.size());
      fout.intField(1, 1);
      fout.writeFields(fmt);
      fmt = new int[] { 8, 24, 24, 24 };
      for (int i = 0; i < _indexs.size(); i++) {
        // SPoint pt=(SPoint)indexs.elementAt(i);
        final SPoint pt = _indexs.elementAt(i);
        fout.intField(0, i + 1);
        fout.doubleField(1, pt.x);
        fout.doubleField(2, pt.y);
        fout.doubleField(3, pt.z);
        fout.writeFields(fmt);

      }
      /** Ecriture des polylignes * */
      fmt = new int[] { 8, 8 };
      fout.intField(0, _pls.length);
      fout.intField(1, 0);
      fout.writeFields(fmt);
      fmt = new int[] { 8, 8, 8 };
      for (int i = 0; i < _pls.length; i++) {
        pts = _pls[i].points;
        final int index1 = _indexs.indexOf(pts[0]) + 1;
        final int index2 = _indexs.indexOf(pts[1]) + 1;
        fout.intField(0, i + 1);
        fout.intField(1, index1);
        fout.intField(2, index2);
        fout.writeFields(fmt);
      }
      /** Ecriture des positions des trous * */
      fmt = new int[] { 8 };
      fout.intField(0, _trs.length);
      fout.writeFields(fmt);
      fmt = new int[] { 8, 24, 24 };
      for (int i = 0; i < _trs.length; i++) {
        final SPoint pt = _trs[i].position;
        fout.intField(0, i + 1);
        fout.doubleField(1, pt.x);
        fout.doubleField(2, pt.y);
        fout.writeFields(fmt);
      }
      /** Ecriture des positions des regions * */
      fmt = new int[] { 8 };
      fout.intField(0, _rgs.length);
      fout.writeFields(fmt);
      fmt = new int[] { 8, 24, 24, 24, 24 };
      for (int i = 0; i < _rgs.length; i++) {
        final SPoint pt = _rgs[i].position;
        final double a = _rgs[i].aireMaxElement;
        fout.intField(0, i + 1);
        fout.doubleField(1, pt.x);
        fout.doubleField(2, pt.y);
        fout.doubleField(3, pt.z);
        fout.doubleField(4, a);
        fout.writeFields(fmt);
      }
      fout.flush();
      fout.close();
    } catch (final IOException ex) {
      CDodico.exception(ex);
    }
  }
}
