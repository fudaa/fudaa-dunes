/*
 * @creation     1998-04-23
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.dunes;

import java.io.FileReader;
import java.io.IOException;

import com.memoire.fu.FuLog;

import org.fudaa.dodico.corba.dunes.IResultatsDunes;
import org.fudaa.dodico.corba.dunes.IResultatsDunesOperations;
import org.fudaa.dodico.corba.geometrie.LTypeElement;
import org.fudaa.dodico.corba.geometrie.SElement;
import org.fudaa.dodico.corba.geometrie.SMaillage;
import org.fudaa.dodico.corba.geometrie.SNoeud;
import org.fudaa.dodico.corba.geometrie.SPoint;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.objet.CDodico;

/**
 * Classe d'implantation de l'interface <code>IResultatsDunes</code> gerant les resultats du code de calcul
 * <code>dunes</code>. Le code de calcul genere un fichier decrivant la structure d'un SMaillage ( ce fichier est lu
 * par la methode <code>litResultatsDunes()</code>).
 *
 * @version $Revision: 1.15 $ $Date: 2006-09-19 14:42:29 $ by $Author: deniger $
 * @author Sofiane Hadji , Bertrand Marchand
 */
public class DResultatsDunes extends DResultats implements IResultatsDunes, IResultatsDunesOperations {
  /**
   * Le nom du fichier (sans extenssion) genere par le code de calcul <code>dunes</code>.
   */
  private String fichier_ = "";
  /**
   * La structure du maillage obtenu a partir du code de calcul.
   */
  private SMaillage sMai_;

  /**
   * Constructeur vide.
   *
   * @see org.fudaa.dodico.calcul.DResultats
   */
  public DResultatsDunes() {
    super();
  }

  /**
   * Modifie le nom du fichier genere par le code de calcul.
   *
   * @param _fichier le nom du fichier sans extension.
   */
  public void setFichier(final String _fichier) {
    fichier_ = _fichier;
  }

  /**
   * Pas implanter completement.
   *
   * @return <code>new DResultatsDunes()</code> avec le meme fichier de resultats.
   */
  public final Object clone() throws CloneNotSupportedException {
    final DResultatsDunes r = (DResultatsDunes) super.clone();
    r.setFichier(fichier_);
    return r;
  }

  /**
   * Permet d'effacer les resultats.
   */
  public void clearResultats() {
    sMai_ = null;
  }

  /**
   * Description de l'objet.
   *
   * @return "DResultatsDunes()"
   */
  public String toString() {
    return "DResultatsDunes()";
  }

  /**
   * Modifie le maillage calcule par le code de calcul.
   *
   * @param _mai le nouveau maillage
   */
  public void maillage(final SMaillage _mai) {
    sMai_ = _mai;
  }

  /**
   * Renvoie le maillage calcule. Si necessaire, lit le fichier genere par le code de calcul.
   *
   * @return la structure du maillage calcule par le code de calcul.
   */
  public SMaillage maillage() {
    if (sMai_ == null) {
      sMai_ = litResultatsDunes(fichier_);
    }
    return sMai_;
  }

  /**
   * Lit le fichier "<code>_fichier</code>.dunes_out" et genere une structure <code>SMaillage</code>.
   *
   * @param _fichier le nom du fichier resultat sans extension.
   * @return la structure <code>SMaillage</code> generee.
   */
  public static SMaillage litResultatsDunes(final String _fichier) {
    SMaillage r = null;
    SElement[] elements = null;
    SNoeud[] noeuds = null;
    try {
      int i, j, nbnoeuds, nbelts;
      // int[] fmt;
      final FortranReader fout = new FortranReader(new FileReader(_fichier + ".dunes_out"));
      // Lecture noeuds
      fout.readFields();
      // fmt = new int[] { 8,8 };
      // fout.readFields(fmt);
      fout.readFields();
      nbnoeuds = fout.intField(0);
      FuLog.all("Dunes_out nombre de noeuds: " + nbnoeuds);
      noeuds = new SNoeud[nbnoeuds];
      for (i = 0; i < nbnoeuds; i++) {
        // mt = new int[] { 8,24,24,24 };
        // fout.readFields(fmt);
        fout.readFields();
        j = fout.intField(0);
        if (j != i + 1) {
          System.err.println("Dunes_out Decalage en ligne : " + (i + 1));
        }
        final SPoint p = new SPoint(fout.doubleField(1), fout.doubleField(2), fout.doubleField(3));
        noeuds[i] = new SNoeud(p);
      }
      // Lecture elements
      fout.readFields();
      fout.readFields();
      // fmt = new int[] { 8 };
      // fout.readFields(fmt);
      fout.readFields();
      nbelts = fout.intField(0);
      FuLog.all("Dunes_out nombre d'elements: " + nbelts);
      elements = new SElement[nbelts];
      // fmt = new int[] { 8,8,8,8,8,8,8,8 };
      for (i = 0; i < nbelts; i++) {
        // fout.readFields(fmt);
        fout.readFields();
        j = fout.intField(0);
        if (j != i + 1) {
          System.err.println("Dunes_out Decalage en ligne : " + (i + 1));
        }
        final int nbNdsEle = fout.intField(1);
        // SNoeud[] nds= new SNoeud[nbNdsEle];
        final int[] idx = new int[nbNdsEle];
        for (j = 0; j < nbNdsEle; j++) {
          // nds[j]= noeuds[fout.intField(j + 2) - 1];
          idx[j] = fout.intField(j + 2) - 1;
        }

        if (nbNdsEle == 3) {
          elements[i] = new SElement(idx, LTypeElement.T3);
        } else {
          elements[i] = new SElement(idx, LTypeElement.T6);
        }
      }
      fout.close();
      r = new SMaillage(elements, noeuds);
      System.err.println("Dunes_out Termine !");
    } catch (final IOException ex) {
      CDodico.exception(DResultatsDunes.class, ex);
    }
    return r;
  }
}
