/**
 * @file         DResultatsOLB.java
 * @creation     1999-11-25
 * @modification $Date: 2006-09-19 14:44:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.olb;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.fudaa.dodico.corba.geometrie.LTypeElement;
import org.fudaa.dodico.corba.geometrie.SElement;
import org.fudaa.dodico.corba.geometrie.SMaillage;
import org.fudaa.dodico.corba.geometrie.SNoeud;
import org.fudaa.dodico.corba.geometrie.SPoint;
import org.fudaa.dodico.corba.olb.IResultatsOlb;
import org.fudaa.dodico.corba.olb.IResultatsOlbOperations;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.objet.CDodico;
/**
 * Les resultats OLB.
 *
 * @version      $Revision: 1.5 $ $Date: 2006-09-19 14:44:22 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class DResultatsOlb
  extends DResultats
  implements IResultatsOlb,IResultatsOlbOperations {
  public void clearResultats(){
  sMai_=null;  
  }
  private String fichier_= "";
  private SMaillage sMai_;
  /**
   * Rien.
   */
  public DResultatsOlb() {
    super();
  }
  public void setFichier(final String _fichier) {
    fichier_= _fichier;
  }
  public final Object clone() throws CloneNotSupportedException {
    final DResultatsOlb r= new DResultatsOlb();
    r.setFichier(fichier_);
    return r;
  }
  public void maillage(final SMaillage _m) {
    sMai_= _m;
  }
  public SMaillage maillage() {
    if (sMai_ == null) {
      sMai_= litResultatsOLB(fichier_);
    }
    return sMai_;
  }
  /**
   * @param _fichier le fichier a lire
   * @return null si erreur.
   */
  public static SMaillage litResultatsOLB(final String _fichier) {
    SMaillage r= null;
    SElement[] elements= null;
    SNoeud[] noeuds= null;
    try {
      int i, j, nbnoeuds, nbelts;
      final FortranReader finp=
        new FortranReader(new FileReader(_fichier + ".olb_out"));
      // Lecture noeuds
      finp.readFields();
      finp.readFields();
      nbnoeuds= finp.intField(0);
      System.out.println("OLB_out nombre de noeuds: " + nbnoeuds);
      noeuds= new SNoeud[nbnoeuds];
      for (i= 0; i < nbnoeuds; i++) {
        finp.readFields();
        j= finp.intField(0);
        if (j != i + 1) {
          System.err.println("OLB_out Decalage en ligne : " + (i + 1));
        }
        final SPoint p=
          new SPoint(
            finp.doubleField(1),
            finp.doubleField(2),
            finp.doubleField(3));
        noeuds[i]= new SNoeud(p);
      }
      // Lecture elements
      finp.readFields();
      finp.readFields();
      finp.readFields();
      nbelts= finp.intField(0);
      System.out.println("OLB_out nombre d'elements: " + nbelts);
      elements= new SElement[nbelts];
      for (i= 0; i < nbelts; i++) {
        finp.readFields();
        j= finp.intField(0);
        if (j != i + 1) {
          System.err.println("OLB_out Decalage en ligne : " + (i + 1));
        }
        final int nbNdsEle= finp.intField(1);
        final int[] nds= new int[nbNdsEle];
        for (j= 0; j < nbNdsEle; j++) {
          nds[j]= finp.intField(j + 2) - 1;
        }
        if (nbNdsEle == 3) {
          elements[i]= new SElement(nds, LTypeElement.T3);
        } else {
          elements[i]= new SElement(nds, LTypeElement.T6);
        }
      }
      finp.close();
      r= new SMaillage(elements, noeuds);
      System.err.println("OLB_out Termine !");
    } catch (final IOException ex) {
      CDodico.exception(DResultatsOlb.class, ex);
    } finally {
      final File finp= new File(_fichier + ".olb_out");
      if (finp.exists()) {
        finp.delete();
      }
    }
    return r;
  }
}
