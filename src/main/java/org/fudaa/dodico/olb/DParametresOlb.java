/**
 * @file         DParametresOLB.java
 * @creation     1999-11-25
 * @modification $Date: 2006-09-19 14:44:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.olb;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.geometrie.SElement;
import org.fudaa.dodico.corba.geometrie.SMaillage;
import org.fudaa.dodico.corba.geometrie.SNoeud;
import org.fudaa.dodico.corba.geometrie.SPoint;
import org.fudaa.dodico.corba.olb.IParametresOlb;
import org.fudaa.dodico.corba.olb.IParametresOlbOperations;

import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.objet.CDodico;
/**
 * Les parametres Olb.
 *
 * @version      $Revision: 1.4 $ $Date: 2006-09-19 14:44:22 $ by $Author: deniger $
 * @author       Bertrand Marchand 
 */
public class DParametresOlb
  extends DParametres
  implements IParametresOlb,IParametresOlbOperations {
  private SMaillage sMaillage_;
  /**
   * initialise le maillage.
   */
  public DParametresOlb() {
    super();
    sMaillage_= new SMaillage();
  }
  
  public final Object clone() throws CloneNotSupportedException {
    return new DParametresOlb();
  }
  public String toString() {
    final String s= "DParametresOLB";
    return s;
  }
  public void maillage(final SMaillage _m) {
    sMaillage_= _m;
  }
  public SMaillage maillage() {
    return sMaillage_;
  }
  /**
   * Ecriture des parametres dans un fichier.
   * @param _fichier le fichier cible
   * @param _m le maillage source
   */
  public static void ecritParametresOLB(final String _fichier, final SMaillage _m) {
    final SNoeud[] nds= _m.noeuds;
    final SElement[] els= _m.elements;
    /*Hashtable nd2Num= new Hashtable(nds.length);
    for (int i= 0; i < nds.length; i++)
      nd2Num.put(nds[i], new Integer(i + 1));*/
    System.out.println("Ecriture de " + _fichier + ".olb_in");
    try {
      final PrintWriter fout= new PrintWriter(new FileWriter(_fichier + ".olb_in"));
      // <NOEUDS>
      fout.println("NOEUDS");
      // <nombre de noeuds>,<nombre de valeurs=1>
      fout.println(nds.length + CtuluLibString.ESPACE + 1);
      // <num�ro de noeud>,<x>,<y>,<z>
      for (int i= 0; i < nds.length; i++) {
        final SPoint pt= nds[i].point;
        fout.println((i + 1) + CtuluLibString.ESPACE + pt.x + CtuluLibString.ESPACE + pt.y + CtuluLibString.ESPACE + pt.z);
      }
      fout.println();
      // <ELEMENTS>
      fout.println("ELEMENTS");
      // <nombre d'�l�ments>
      fout.println(els.length);
      // <num�ro d'�l�ment>,<nombre de noeuds>,<num�ros de noeuds>
      for (int i= 0; i < els.length; i++) {
        final int[] ndsEle= els[i].noeudsIdx;
        fout.print((i + 1) + CtuluLibString.ESPACE + ndsEle.length);
        for (int j= 0; j < ndsEle.length; j++) {
          fout.print(CtuluLibString.ESPACE + (ndsEle[j]+1));
        }
        fout.println();
      }
      fout.flush();
      fout.close();
    } catch (final IOException ex) {
      CDodico.exception(ex);
    }
  }
}
