/**
 * @file         DCalculOLB.java
 * @creation     1999-11-25
 * @modification $Date: 2006-09-19 14:44:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.olb;
import java.io.File;
import java.io.InputStreamReader;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.olb.ICalculOlb;
import org.fudaa.dodico.corba.olb.ICalculOlbOperations;
import org.fudaa.dodico.corba.olb.IParametresOlb;
import org.fudaa.dodico.corba.olb.IParametresOlbHelper;
import org.fudaa.dodico.corba.olb.IResultatsOlb;
import org.fudaa.dodico.corba.olb.IResultatsOlbHelper;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.objet.CDodico;
/**
 * Une encapsulation du programme d'optimisation de largeur de bande ecrit en
 * C++.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 14:44:22 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class DCalculOlb extends DCalcul implements ICalculOlb,ICalculOlbOperations {
  private StringBuffer traceBuffer_= new StringBuffer();
  private boolean succes_;
  private boolean arretDemande_;
  private Process proc_; // Sous process d'ex�cution du programme.
  /**
   * intialise les fichiers d'extension.
   */
  public DCalculOlb() {
    super();
    setFichiersExtensions(new String[] { ".olb_in", ".olb_out" });
  }
  public final Object clone() throws CloneNotSupportedException {
    return new DCalculOlb();
  }
  public String toString() {
    return "DCalculOlb()";
  }
  public String description() {
    return "OLB, serveur d'optimisation de largeur de bande: "
      + super.description();
  }
  /**
   * Ex�cution de OLB.
   */
  public void calcul(final IConnexion _c) {
    succes_= false;
    if (!verifieConnexion(_c)) {
      return;
    }
    final IParametresOlb params= IParametresOlbHelper.narrow(parametres(_c));
    if (params == null){
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
      return;
    }
    final IResultatsOlb results= IResultatsOlbHelper.narrow(resultats(_c));
    if (results == null){
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
      return;
    }
    results.clearResultats();
    log(_c, "lancement du calcul");
    traceBuffer_= new StringBuffer();
    final String os= System.getProperty("os.name");
    final String path= cheminServeur();
    final String fichier= "olb" + _c.numero();
    try {
      if (arretDemande_) {
        return;
      }
      System.out.println("Ecriture des param�tres pour " + fichier);
      DParametresOlb.ecritParametresOLB(path + fichier, params.maillage());
      if (arretDemande_) {
        return;
      }
      System.out.println("Ex�cution de OLB pour " + fichier);
      String exe;
      if (os.startsWith("Windows")) {
        exe= "olb.exe";
      } else {
        exe= "olb-" + os + ".x";
      }
      //        String[] cmd;
      //        int icmd=0;
      //        if( os.startsWith("Windows") ) {
      //          cmd=new String[4];
      //          cmd[icmd++]=path+"launcher.bat";
      //          if( path.indexOf(':')!=-1 ) {
      //            // lettre de l'unite (ex: "C:")
      //            cmd[icmd++]=path.substring(0, path.indexOf(':')+1);
      //            // chemin du serveur
      //            cmd[icmd++]=path.substring(path.indexOf(':')+1);
      //          }
      //          else {
      //            // si pas de lettre dans le chemin
      //            cmd[icmd++]="fake_cmd";
      //            cmd[icmd++]=path;
      //          }
      //          System.out.println(cmd[0]+" "+cmd[1]+" "+cmd[2]);
      //        } else {
      //          cmd=new String[2];
      //          cmd[icmd++]=path+"launcher.sh";
      //          System.out.println(cmd[0]);
      //        }
      //        cmd[icmd++]=fichier;
      final String[] cmd= new String[3];
      cmd[0]= path + exe;
      cmd[1]= fichier + ".olb_in";
      cmd[2]= fichier + ".olb_out";
      //      CExec ex=new CExec();
      //      ex.setCommand(cmd);
      //      ex.setOutStream(System.out);
      //      ex.setErrStream(System.err);
      //      ex.exec();
      if (arretDemande_) {
        return;
      }
      proc_= Runtime.getRuntime().exec(cmd, null, new File(path));
      if (arretDemande_) {
        proc_.destroy();
      } else {
        proc_.waitFor();
      }
      if (arretDemande_) {
        return;
      }
      final InputStreamReader perr= new InputStreamReader(proc_.getErrorStream());
      final InputStreamReader pout= new InputStreamReader(proc_.getInputStream());
      int ch;
      // Trace d'ex�cution.
      while ((ch= pout.read()) != -1) {
        traceBuffer_.append((char)ch);
      }
      // Erreurs sur stream Error
      String sResult= "";
      while ((ch= perr.read()) != -1) {
        sResult += (char)ch;
      }
      traceBuffer_.append(sResult);
      // Succ�s du calcul si aucune erreur et fichier r�sultats existant.
      if (proc_.exitValue() == 0) {
        succes_= new File(path + fichier + ".olb_out").length() != 0;
      }
      // Le calcul s'est mal d�roul�.
      if (!succes_) {
        System.out.println("Erreurs :" + sResult);
        return;
      }
      results.setFichier(path + fichier);
      System.out.println("Fin du calcul");
      log(_c, "calcul termin�");
    } catch (final Exception ex) {
      log(_c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    } finally {
      if (arretDemande_) {
        traceBuffer_.append("\n*** Interruption utilisateur ***\n");
        arretDemande_= false;
      }
    }
  }
  /**
   * Arret demand� du calcul en cours.
   */
  public void setArretDemande() {
    arretDemande_= true;
    // Arret du sous process s'il est en cours.
    if (proc_ != null) {
      proc_.destroy();
    }
  }
  /**
   * Annulation de l'arret demand�.
   */
  public void clearArretDemande() {
    arretDemande_= false;
  }
  /**
   * Retourne la trace d'ex�cution sous forme de chaine. Les lignes sont
   * s�par�es par des caract�res de fin de ligne "\n".
   *
   * @return La trace d'ex�cution.
   */
  public String traceExecution() {
    return traceBuffer_.toString();
  }
  /**
   * Retourne l'�tat de sortie du calcul. Le calcul s'est correctement d�roul�
   * si des r�sultats existent.
   *
   * @return <i>true</i> Le calcul s'est bien d�roul�. <i>false</i> sinon.
   */
  public boolean estOK() {
    return succes_;
  }
}
